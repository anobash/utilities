#!/bin/bash
ISO="tor af cn ir jp th sd np eg tw tr bd az kr ro jm vn br ao in ar"

### Set PATH ###
IPT=/sbin/iptables
IPT6=/sbin/ip6tables
IPS=/sbin/ipset
WGET=/usr/bin/wget
EGREP=/bin/egrep
DENY=/etc/nginx/conf.d/blockusers.conf

### No editing below ###
SPAMLIST="countrydrop"
SPAMLIST6="countrydrop6"
ZONEROOT="/root/iptables"
DLROOT="http://www.ipdeny.com/ipblocks/data/countries"
TORLIST="https://www.dan.me.uk/torlist/"

purgeTable() {
        $IPT -D INPUT -m set --match-set $SPAMLIST src -j DROP
        $IPT -D OUTPUT -m set --match-set $SPAMLIST src -j DROP
        $IPT -D FORWARD -m set --match-set $SPAMLIST src -j DROP
        $IPS flush $SPAMLIST
        $IPS destroy $SPAMLIST
        $IPT6 -F
        $IPS flush $SPAMLIST6
        $IPS destroy $SPAMLIST6
}


purgeTable
/bin/rm -f $ZONEROOT/*.zone
/bin/rm -f $DENY

# create a new iptables list
$IPS create $SPAMLIST hash:net
$IPS create $SPAMLIST6 hash:ip family inet6

for c in $ISO
do
        echo "# Forbiding $c" >> $DENY
        # local zone file
        tDB=$ZONEROOT/$c.zone

        if [ "$c" = "tor" ]
        then
                echo "getting $TORLIST"
                $WGET -q -O $tDB $TORLIST

        else
                # get fresh zone file
                echo "getting $DLROOT/$c.zone"
                $WGET -q -O $tDB $DLROOT/$c.zone
        fi

        # country specific log message
        SPAMDROPMSG="$c Country Drop"

        # get
        BADIPS=$(egrep -v "^#|^$" $tDB)
        for ipblock in $BADIPS
        do
            if [[ $ipblock == *":"* ]]
            then
                $IPS add $SPAMLIST6 $ipblock
                echo "deny $ipblock;" >> $DENY
            else
                $IPS add $SPAMLIST $ipblock
                echo "deny $ipblock;" >> $DENY
            fi
        done
done

/etc/init.d/nginx force-reload

# Drop everything
$IPT -I INPUT -m set --match-set $SPAMLIST src -j DROP
$IPT -I OUTPUT -m set --match-set $SPAMLIST src -j DROP
$IPT -I FORWARD -m set --match-set $SPAMLIST src -j DROP
$IPT6 -I INPUT -m set --match-set $SPAMLIST6 src -j DROP
$IPT6 -I OUTPUT -m set --match-set $SPAMLIST6 src -j DROP
$IPT6 -I FORWARD -m set --match-set $SPAMLIST6 src -j DROP

exit 0