#!/bin/sh

SPATH="/home/user/tosave"
DIST="user@10.0.3:/home/user/saves/"
LOG="/tmp/rsync.log"

#####################
# DO NOT EDIT BELOW #
#####################

NOW=$(date +"%Y%m%d")
echo "Starting rsync $NOW" >> $LOG

# mode recup #
/usr/bin/rsync -vrz --size-only --delete-after --exclude '*forceupdate.file' ${SPATH}/* ${DIST} > ${LOG}
/usr/bin/scp ${SPATH}/forceupdate.file ${DIST}
echo "Ending rsync" >> $LOG

exit 0