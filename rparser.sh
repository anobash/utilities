#!/bin/bash

R='\033[0;31m'
G='\033[0;32m'
y='\033[1;33m'
B='\033[0;34m'
NC='\033[0m'

function join { local IFS=', '; echo "$*"; }

function getdetals {
   local reason=()
   local line=$1
   local target=${line:11}
   case ${line:0:1} in
      f ) type="file";;
      d ) type="directory";;
      L ) type="symlink";;
      D ) type="device";;
      S ) type="special";;
      * ) type="unknown";;
   esac
   if [ "${line:1:1}" = "c" ]; then reason+=('checksum'); fi
   if [ "${line:2:1}" = "s" ]; then reason+=('size'); fi
   if [ "${line:3:1}" = "t" -o "${line:3:1}" = "T" ]; then reason+=('time'); fi
   if [ "${line:4:1}" = "p" ]; then reason+=('permission'); fi
   if [ "${line:5:1}" = "o" ]; then reason+=('owner'); fi
   if [ "${line:6:1}" = "g" ]; then reason+=('group'); fi
   if [ "${line:1:9}" = "+++++++++" ]; then reason+=('added'); fi
   echo "${type} ${target} ($(join ${reason}))"
}

if [ $# -eq 0 ]
then
   echo -e "${R}Error${NC} No file given"
   exit 0
elif [ ! -f $1 ]
then
   echo -e "${R}Error${NC} Cannot find ${B}$1${NC}"
   exit 0
else
   LOGFILE=$1
fi

declare -A count=([down]=0 [up]=0 [del]=0 [ign]=0 [mod]=0)

echo -e "*** Loading ${LOGFILE} ***"

IFS=$'\n'
for line in $(cat ${LOGFILE})
do
   op=${line:0:1}
   line=${line:1}
   case ${op} in
      [\>] ) action="${B}<== `getdetals ${line}`${NC}"; ((count[down]++));;
      [\<] ) action="${G}==> `getdetals ${line}`${NC}"; ((count[up]++));;
      [\*] ) msg=${line:0:10};action="${R}${msg^^} ${line:11}${NC}"; ((count[del]++));;
      [\.] ) action="${Y}IGNORED `getdetals ${line}`"; ((count[ign]++));;
      c ) action="${Y}MODIFIED `getdetals ${line}`${NC}"; ((count[mod]++));;
   esac
   echo -e $action
done

echo "*******************************************"
echo " Downloaded: ${count[down]}"
echo " Uploaded: ${count[up]}"
echo " Deleted: ${count[del]}"
echo " Ignored: ${count[ign]}"
echo " Modified: ${count[mod]}"