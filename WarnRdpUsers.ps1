# Short script to send message to all users connected on rdp
#
# Needs elevated privileges
Param
(
    [string]$subject="empty",
    [string]$body="empty",
    [switch]$disc
)

if ($subject -eq "empty" -or $body -eq "empty")
{
    Write-Warning "Usage: c:\admin\WarnRdpUsers.ps1 [-disc] ""Subject of the warning"" ""Body of the warning"""
    exit
}
Import-Module RemoteDesktop

$users = Get-RDUserSession
if ($users.count -eq 0)
{
    Write-Warning "No user to warn"
    exit
}
Write-Host -ForegroundColor Yellow "Subject is : " -NoNewline
Write-Host $subject
Write-Host -ForegroundColor Yellow "Body is : " -NoNewline
Write-Host $body
Write-Host -ForegroundColor Red "Send message to $($users.count) users ? (y/N)" -NoNewline
$proceed = Read-Host 
if ($proceed -ne "y" -and $proceed -ne "Y")
{
    Write-Warning "Aborted..."
    exit
}
$subject = "$($env:UserName)> $($subject)"
foreach ($user in $users)
{
    $Uname = $user.UserName
    $Uhost = $user.HostServer
    $Uid = $user.UnifiedSessionId
    Write-Host "$subject sent to $Uname"
    Send-RDUserMessage -HostServer $Uhost -UnifiedSessionID $Uid -MessageTitle $subject -MessageBody $body
    if ($disc)
    {
        Invoke-RDUserLogoff -Force -HostServer $Uhost -UnifiedSessionID $Uid
    }
}
Write-Host "Message sent"